/**
 * Created by Andrei on 12.07.2016.
 */
$(document).ready(function() {
    $('.fa-angle-down').mouseenter(function() {
        $(this).animate({"bottom" : "-110px"}, 'slow');

    });
    $('.fa-angle-down').mouseleave(function() {
        $(this).animate({"bottom" : "0"}, 'fast');

    });
});